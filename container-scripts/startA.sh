#!/bin/bash
# process running
if [ "${SERVER_TYPE}" == "adminserver" ]; then
  SVR="AdminServer"
  
elif [ "${SERVER_TYPE}" == "managedserver" ]; then
  SVR=${HOSTNAME}
  
fi


# process running checking !!!
#PID=`ps -ef|grep java|grep ${SVR}|awk '{print $2}'`
#if [ "$PID" != "" ]
#then
# echo .
# echo "${SVR}"_[pid:"${PID}"] Process Is Running !!!
# echo .
#  exit
#fi

## check config directory whether this instance was started first time.
## /oracle/domains/base_domain/config directory is mapped to Persistent volume
if [ "${SERVER_TYPE}" == "adminserver" ]; then
  if [ ! -f "/oracle/domains/base_domain/config/config.xml" ]; then
    ## config does not exist. started first time
    ## mv config_org directory to config(pv)
    echo "<------- config does not exist ------->"
    if [ ! -f "/oracle/domains/base_domain/config" ]; then
      mkdir -p /oracle/domains/base_domain/config  
    fi
    cp -R /oracle/domains/base_domain/config_org/* /oracle/domains/base_domain/config/
  fi
fi


USER_MEM_ARGS="-Djava.security.egd=file:/dev/./urandom -D${SVR} -Xms${MIN_HEAP}m -Xmx${MAX_HEAP}m -XX:NewSize=${MIN_NEW}M -XX:MaxNewSize=${MAX_NEW}M -XX:SurvivorRatio=${SURVIVOR_RATIO} -XX:TargetSurvivorRatio=90 -XX:MaxTenuringThreshold=15 -XX:ParallelGCThreads=8 -XX:+UseParallelOldGC -XX:-UseAdaptiveSizePolicy -XX:+DisableExplicitGC -verbose:gc" ## -Xloggc:/oracle/logs/gc.out"

export USER_MEM_ARGS
export JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.threadpool.MinPoolSize=${MIN_POOL_SIZE}"

## PostgreSQL JDBC
#export PRE_CLASSPATH="${ORACLE_HOME}/wlserver_10.3/server/lib/postgresql-42.1.1.jre7.jar:${PRE_CLASSPATH}"


#mv ./logs/nohup.${SVR}.out ./logs/${SVR}/nohup.${SVR}_`date +'%Y%m%d_%H%M%S'`.out
#mv ./logs/gc.${SVR}.out ./logs/${SVR}/gc.${SVR}_`date +'%Y%m%d_%H%M%S'`.out

#touch ${LOG_HOME}/nohup.out


## if adminserver, deploy jdbc Datasource and Application
if [ "${SERVER_TYPE}" == "adminserver" ]; then
  . /oracle/domains/base_domain/scripts/datasource.sh
  injectDataSource

  . /oracle/domains/base_domain/scripts/deployment.sh
  injectWARDeployment
  injectEARDeployment

fi


if [ "${SERVER_TYPE}" == "adminserver" ]; then
#  echo "===================> SERVER_TYPE = adminserver ==============" >> ${LOG_HOME}/nohup.out
  echo "===================> SERVER_TYPE = adminserver =============="
#  nohup ${DOMAIN_HOME}/scripts/checkDeletedManagedServer.sh &
  ${DOMAIN_HOME}/bin/startWebLogic.sh
  
elif [ "${SERVER_TYPE}" == "managedserver" ]; then
  echo "===================> SERVER_TYPE = managedserver ==============" >> ${LOG_HOME}/nohup.out
  export POD_IP=`ifconfig eth0 | grep 'inet '`
  export POD_IP=`echo ${POD_IP} | cut -d ' ' -f 2`
  echo "===================> POD_IP = ${POD_IP} ==================" >> ${LOG_HOME}/nohup.out
  
  # Make boot.properties
  mkdir -p /oracle/domains/base_domain/servers/${HOSTNAME}/security
  echo "username=${WLS_ADMIN}" >> /oracle/domains/base_domain/servers/${HOSTNAME}/security/boot.properties
  echo "password=${WLS_PASSWD}" >> /oracle/domains/base_domain/servers/${HOSTNAME}/security/boot.properties
  
  
  ${DOMAIN_HOME}/scripts/checkAdminServerAndInit.sh
  ${DOMAIN_HOME}/bin/startManagedWebLogic.sh ${HOSTNAME} t3://${ADMINSERVER_URL}
  
fi


