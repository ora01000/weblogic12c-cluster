#!/bin/bash

## check weblogic server is running
sleep 120

while :
do
    state=`exec ${ORACLE_HOME}/oracle_common/common/bin/wlst.sh ${DOMAIN_HOME}/scripts/mon_server.py | tail -1`
    if [[ "$state" == *RUNNING* ]];
    then
        ${ORACLE_HOME}/oracle_common/common/bin/wlst.sh ${DOMAIN_HOME}/scripts/purge_managed_server.py
    fi
    sleep 60
done



