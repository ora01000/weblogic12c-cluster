#!/bin/bash

## check weblogic server is running

while :
do
    echo "<-------------- Check whether AdminServer is RUNNING -------------->"
    state=`exec ${ORACLE_HOME}/oracle_common/common/bin/wlst.sh ${DOMAIN_HOME}/scripts/mon_server.py | tail -1`
    if [[ "$state" == *RUNNING* ]];
    then
    	echo "<----------- AdminServer is RUNNING -------------->"
        break
    else
##    	echo "<----------- AdminServer is NOT RUNNING ---------->"
        sleep 2
    fi
done

##echo "<-------------------------- WebLogic Server Running !! -------------------------->"

## create cluster/datasource & deploy application

if [ "${SERVER_TYPE}" == "adminserver" ]; then
##  ${ORACLE_HOME}/oracle_common/common/bin/wlst.sh ${DOMAIN_HOME}/scripts/init_cluster.py && ${ORACLE_HOME}/oracle_common/common/bin/wlst.sh ${DOMAIN_HOME}/scripts/deploy_app.py
##  ${ORACLE_HOME}/oracle_common/common/bin/wlst.sh ${DOMAIN_HOME}/scripts/deploy_app.py
  echo "AdminServer ---------> Do Nothing in this script"
elif [ "${SERVER_TYPE}" == "managedserver" ]; then
  ${ORACLE_HOME}/oracle_common/common/bin/wlst.sh ${DOMAIN_HOME}/scripts/deploy_server.py
fi