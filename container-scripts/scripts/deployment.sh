#!/bin/bash
## create Application Deployment

function createWARDeployment() {

  echo "----------->${1}"
  local DEPLOYMENT_NAME=$(basename ${1})
  
  APP_STRING="<!-- APP_DEPLOYMENT -->\n  <app-deployment>\n    <name>${DEPLOYMENT_NAME}</name>\n    <target>base_cluster</target>\n    <module-type>war</module-type>\n    <source-path>${1}</source-path>\n    <security-dd-model>DDOnly</security-dd-model>\n    <staging-mode xsi:nil=\"true\"></staging-mode>\n    <plan-staging-mode xsi:nil=\"true\"></plan-staging-mode>\n    <cache-in-app-directory>false</cache-in-app-directory>\n  </app-deployment>"
  sed -i "s|<!-- APP_DEPLOYMENT -->|${APP_STRING}|" /oracle/domains/base_domain/config/config.xml
  
}

function createEARDeployment() {

  echo "----------->${1}"
  local DEPLOYMENT_NAME=$(basename ${1})
  
  APP_STRING="<!-- APP_DEPLOYMENT -->\n  <app-deployment>\n    <name>${DEPLOYMENT_NAME}</name>\n    <target>base_cluster</target>\n    <module-type>ear</module-type>\n    <source-path>${1}</source-path>\n    <security-dd-model>DDOnly</security-dd-model>\n    <staging-mode xsi:nil=\"true\"></staging-mode>\n    <plan-staging-mode xsi:nil=\"true\"></plan-staging-mode>\n    <cache-in-app-directory>false</cache-in-app-directory>\n  </app-deployment>"
  sed -i "s|<!-- APP_DEPLOYMENT -->|${APP_STRING}|" /oracle/domains/base_domain/config/config.xml
  
}

function injectWARDeployment() {
  FILES=`ls -l /home/weblogic/deployments | awk '{print $9}' | grep .war`
  for f in ${FILES}
  do
    echo "------>>>>> ${f}"
    $(createWARDeployment "/home/weblogic/deployments/${f}")
  done
}


function injectEARDeployment() {
  FILES=`ls -l /home/weblogic/deployments | awk '{print $9}' | grep .ear`
  for f in ${FILES}
  do
    echo "------>>>>> ${f}"
    $(createEARDeployment "/home/weblogic/deployments/${f}")
  done
}

