import thread
import time

## Global Properties
adminURL = os.environ['ADMINSERVER_URL']
adminUsername = os.environ['WLS_ADMIN']
adminPassword = os.environ['WLS_PASSWD']

# Connect to AdminServer.
connect(adminUsername, adminPassword, adminURL)
domainRuntime()

slrBean = cmo.lookupServerLifeCycleRuntime('AdminServer')
status = slrBean.getState()
print status