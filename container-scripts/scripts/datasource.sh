#!/bin/bash
## create Datasource from /home/weblogic/datasources/**.properties

function find_props() {
  local propsName="${1}"
  local propsFile="${2}"
  
  local line=`grep "${1}=" ${2}`
  echo ${line#*=}
}

function createDatasource() {

  local DS_NAME=$(find_props "ds.name" "${1}")
  local DS_JNDI=$(find_props "ds.jndi.name" "${1}")
  local DS_URL=$(find_props "ds.url" "${1}")
  local DS_DRIVER=$(find_props "ds.driver" "${1}")
  local DS_USERNAME=$(find_props "ds.username" "${1}")
  local DS_PASSWORD=$(find_props "ds.password" "${1}")
  local DS_MINIMUM_CAPACITY=$(find_props "ds.minimum_capacity" "${1}")
  local DS_MAXIMUM_CAPACITY=$(find_props "ds.maximum_capacity" "${1}")
  local DS_INITIAL_CAPACITY=$(find_props "ds.initial_capacity" "${1}")
  local DS_TEST_CONNECTIONS_ON_RESERVE=$(find_props "ds.test_connections_on_reserve" "${1}")
  local DS_TEST_FREQUENCY=$(find_props "ds.test_frequency" "${1}")
  local DS_TEST_TABLE_NAME=$(find_props "ds.test_table_name" "${1}")
  local DS_SECONDS_TO_TRUST_AN_IDLE_POOL_CONNECTION=$(find_props "ds.seconds_to_trust_an_idle_pool_connection" "${1}")
  local DS_SHRINK_FREQUENCY=$(find_props "ds.shrink_frequency" "${1}")
  local DS_CONNECTION_CREATION_RETRY_FREQUENCY=$(find_props "ds.connection_creation_retry_frequency" "${1}")
  local DS_INACTIVE_CONNECTION_TIMEOUT=$(find_props "ds.inactive_connection_timeout" "${1}")
  local DS_CONNECTION_RESERVE_TIMEOUT=$(find_props "ds.connection_reserve_timeout" "${1}")
  local DS_PROFILE_CONNECTION_LEAK=$(find_props "ds.profile_connection_leak" "${1}")
  
  ## Password Encrypt
  . /oracle/domains/base_domain/bin/setDomainEnv.sh
  DS_PASSWORD=`java -Djava.security.egd=file:/dev/./urandom weblogic.security.Encrypt ${DS_PASSWORD}`
  
  if [ "${DS_PROFILE_CONNECTION_LEAK}" == "true" ]; then
    DS_PROFILE_CONNECTION_LEAK="4"
  else
    DS_PROFILE_CONNECTION_LEAK="0"
  fi
  
  
  DS_STRING="<!-- JDBC_SYSTEM_RESOURCE -->\n  <jdbc-system-resource>\n    <name>${DS_NAME}</name>\n    <target>base_cluster</target>\n    <descriptor-file-name>jdbc/${DS_NAME}-jdbc.xml</descriptor-file-name>\n  </jdbc-system-resource>\n"
  sed -i "s|<!-- JDBC_SYSTEM_RESOURCE -->|${DS_STRING}|" /oracle/domains/base_domain/config/config.xml
  
  ## make jdbc/${DS_NAME}-jdbc.xml
  cat > /oracle/domains/base_domain/config/jdbc/${DS_NAME}-jdbc.xml - <<API
<?xml version='1.0' encoding='UTF-8'?>
<jdbc-data-source xmlns="http://xmlns.oracle.com/weblogic/jdbc-data-source" xmlns:sec="http://xmlns.oracle.com/weblogic/security" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wls="http://xmlns.oracle.com/weblogic/security/wls" xsi:schemaLocation="http://xmlns.oracle.com/weblogic/jdbc-data-source http://xmlns.oracle.com/weblogic/jdbc-data-source/1.0/jdbc-data-source.xsd">
  <name>${DS_NAME}</name>
  <jdbc-driver-params>
    <url>${DS_URL}</url>
    <driver-name>${DS_DRIVER}</driver-name>
    <properties>
      <property>
        <name>user</name>
        <value>${DS_USERNAME}</value>
      </property>
    </properties>
    <password-encrypted>${DS_PASSWORD}</password-encrypted>
  </jdbc-driver-params>
  <jdbc-connection-pool-params>
    <initial-capacity>${DS_INITIAL_CAPACITY}</initial-capacity>
    <max-capacity>${DS_MAXIMUM_CAPACITY}</max-capacity>
    <min-capacity>${DS_MINIMUM_CAPACITY}</min-capacity>
    <shrink-frequency-seconds>${DS_SHRINK_FREQUENCY}</shrink-frequency-seconds>
    <connection-creation-retry-frequency-seconds>${DS_CONNECTION_CREATION_RETRY_FREQUENCY}</connection-creation-retry-frequency-seconds>
    <connection-reserve-timeout-seconds>${DS_CONNECTION_RESERVE_TIMEOUT}</connection-reserve-timeout-seconds>
    <test-frequency-seconds>${DS_TEST_FREQUENCY}</test-frequency-seconds>
    <test-connections-on-reserve>${DS_TEST_CONNECTIONS_ON_RESERVE}</test-connections-on-reserve>
    <inactive-connection-timeout-seconds>${DS_INACTIVE_CONNECTION_TIMEOUT}</inactive-connection-timeout-seconds>
    <test-table-name>${DS_TEST_TABLE_NAME}</test-table-name>
    <seconds-to-trust-an-idle-pool-connection>${DS_SECONDS_TO_TRUST_AN_IDLE_POOL_CONNECTION}</seconds-to-trust-an-idle-pool-connection>
    <profile-type>${DS_PROFILE_CONNECTION_LEAK}</profile-type>
  </jdbc-connection-pool-params>
  <jdbc-data-source-params>
    <jndi-name>${DS_JNDI}</jndi-name>
    <global-transactions-protocol>TwoPhaseCommit</global-transactions-protocol>
  </jdbc-data-source-params>
</jdbc-data-source>
API

  chmod 777 /oracle/domains/base_domain/config/jdbc/${DS_NAME}-jdbc.xml
  
}

function injectDataSource() {
#  FILES=/home/weblogic/datasources/*.properties
  FILES=`ls -l /home/weblogic/datasources | awk '{print $9}' | grep .properties`
  for f in ${FILES}
  do
    echo "------->>>> ${f}"
    $(createDatasource "/home/weblogic/datasources/${f}")
  done
}


