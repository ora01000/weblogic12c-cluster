#!/usr/bin/python
import re
import os
import time
import getopt
import sys

## Global Properties
adminURL = os.environ['ADMINSERVER_URL']
adminUsername = os.environ['WLS_ADMIN']
adminPassword = os.environ['WLS_PASSWD']
clusterName = 'base_cluster'

print "========= Init Cluster ============"
print "==> ADMINSERVER_URL = " + adminURL
print "==> WLS_ADMIN = " + adminUsername
print "==> WLS_PASSWD = " + adminPassword

# Connect to AdminServer.
connect(adminUsername, adminPassword, adminURL)
connect(adminUsername, adminPassword, adminURL)

edit()
startEdit()

# Create cluster.
cd('/')
cmo.createCluster(clusterName)

cd('/Clusters/' + clusterName)
cmo.setClusterMessagingMode('unicast')

save()
activate()

disconnect()
exit()