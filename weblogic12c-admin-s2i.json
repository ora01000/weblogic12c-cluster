{
    "kind": "Template",
    "apiVersion": "v1",
    "metadata": {
        "annotations": {
            "iconClass": "icon-oracle",
            "tags": "oracle,javaee,java,jboss",
            "version": "1.0.0",
            "openshift.io/display-name": "WebLogic 12c (no https)",
            "openshift.io/provider-display-name": "Timegate, Inc.",
            "description": "An example WebLogic 12c application.",
            "template.openshift.io/long-description": "This template defines resources needed to develop Oracle WebLogic 12c(12.2.1.3.0) based application, including a build configuration, application deployment configuration and insecure communication using http.",
            "template.openshift.io/documentation-url": "https://access.redhat.com/documentation/en/red-hat-jboss-enterprise-application-platform/",
            "template.openshift.io/support-url": "https://access.redhat.com"
        },
        "name": "weblogic12c-admin-s2i"
    },
    "labels": {
        "template": "weblogic12c-admin-s2i",
        "xpaas": "1.0.0"
    },
    "message": "A new WebLogic 12c based application has been created in your project.",
    "parameters": [
        {
            "displayName": "Application Name",
            "description": "The name for the application.",
            "name": "APPLICATION_NAME",
            "value": "weblogic-app",
            "required": true
        },
        {
            "displayName": "Custom http Route Hostname",
            "description": "Custom hostname for http service route.  Leave blank for default hostname, e.g.: <application-name>-<project>.<default-domain-suffix>",
            "name": "HOSTNAME_HTTP",
            "value": "",
            "required": false
        },
        {
            "displayName": "Git Repository URL",
            "description": "Git source URI for application",
            "name": "SOURCE_REPOSITORY_URL",
            "value": "https://gitlab.com/ora01000/j2ee.git",
            "required": true
        },
        {
            "displayName": "Git Reference",
            "description": "Git branch/tag reference",
            "name": "SOURCE_REPOSITORY_REF",
            "value": "master",
            "required": false
        },
        {
            "displayName": "Context Directory",
            "description": "Path within Git project to build; empty for root project directory.",
            "name": "CONTEXT_DIR",
            "value": "/",
            "required": false
        },
        {
            "displayName": "Github Webhook Secret",
            "description": "GitHub trigger secret",
            "name": "GITHUB_WEBHOOK_SECRET",
            "from": "[a-zA-Z0-9]{8}",
            "generate": "expression",
            "required": true
        },
        {
            "displayName": "Generic Webhook Secret",
            "description": "Generic build trigger secret",
            "name": "GENERIC_WEBHOOK_SECRET",
            "from": "[a-zA-Z0-9]{8}",
            "generate": "expression",
            "required": true
        },
        {
            "displayName": "ImageStream Namespace",
            "description": "Namespace in which the ImageStreams for Red Hat Middleware images are installed. These ImageStreams are normally installed in the openshift namespace. You should only need to modify this if you've installed the ImageStreams in a different namespace/project.",
            "name": "IMAGE_STREAM_NAMESPACE",
            "value": "openshift",
            "required": true
        },
        {
            "displayName": "Maven mirror URL",
            "description": "Maven mirror to use for S2I builds",
            "name": "MAVEN_MIRROR_URL",
            "value": "",
            "required": false
        },
        {
            "description": "List of directories from which archives will be copied into the deployment folder. If unspecified, all archives in /target will be copied.",
            "name": "ARTIFACT_DIR",
            "value": "",
            "required": false
        }
    ],
    "objects": [
        {
            "kind": "Service",
            "apiVersion": "v1",
            "spec": {
                "ports": [
                    {
                        "port": 7001,
                        "targetPort": 7001
                    }
                ],
                "selector": {
                    "deploymentConfig": "${APPLICATION_NAME}-adminserver"
                }
            },
            "metadata": {
                "name": "${APPLICATION_NAME}-adminserver",
                "labels": {
                    "application": "${APPLICATION_NAME}"
                },
                "annotations": {
                    "description": "The WebLogic AdminConsole Port."
                }
            }
        },
        {
            "kind": "Route",
            "apiVersion": "v1",
            "id": "${APPLICATION_NAME}-adminserver",
            "metadata": {
                "name": "${APPLICATION_NAME}-adminserver",
                "labels": {
                    "application": "${APPLICATION_NAME}"
                },
                "annotations": {
                    "description": "Route for WebLogic AdminConsole service."
                }
            },
            "spec": {
                "host": "${HOSTNAME_HTTP}",
                "to": {
                    "name": "${APPLICATION_NAME}-adminserver"
                }
            }
        },
        {
            "kind": "ImageStream",
            "apiVersion": "v1",
            "metadata": {
                "name": "${APPLICATION_NAME}",
                "labels": {
                    "application": "${APPLICATION_NAME}"
                }
            }
        },
        {
            "kind": "BuildConfig",
            "apiVersion": "v1",
            "metadata": {
                "name": "${APPLICATION_NAME}",
                "labels": {
                    "application": "${APPLICATION_NAME}"
                }
            },
            "spec": {
                "source": {
                    "type": "Git",
                    "git": {
                        "uri": "${SOURCE_REPOSITORY_URL}",
                        "ref": "${SOURCE_REPOSITORY_REF}"
                    },
                    "contextDir": "${CONTEXT_DIR}"
                },
                "strategy": {
                    "type": "Source",
                    "sourceStrategy": {
                        "env": [
                            {
                                "name": "MAVEN_MIRROR_URL",
                                "value": "${MAVEN_MIRROR_URL}"
                            },
                            {
                                "name": "ARTIFACT_DIR",
                                "value": "${ARTIFACT_DIR}"
                            }
                        ],
                        "forcePull": true,
                        "from": {
                            "kind": "ImageStreamTag",
                            "namespace": "${IMAGE_STREAM_NAMESPACE}",
                            "name": "weblogic-cluster-openshift:12.2.1.3.0"
                        }
                    }
                },
                "output": {
                    "to": {
                        "kind": "ImageStreamTag",
                        "name": "${APPLICATION_NAME}:latest"
                    }
                },
                "triggers": [
                    {
                        "type": "GitHub",
                        "github": {
                            "secret": "${GITHUB_WEBHOOK_SECRET}"
                        }
                    },
                    {
                        "type": "Generic",
                        "generic": {
                            "secret": "${GENERIC_WEBHOOK_SECRET}"
                        }
                    },
                    {
                        "type": "ImageChange",
                        "imageChange": {}
                    },
                    {
                        "type": "ConfigChange"
                    }
                ]
            }
        },
        {
            "kind": "DeploymentConfig",
            "apiVersion": "v1",
            "metadata": {
                "name": "${APPLICATION_NAME}-adminserver",
                "labels": {
                    "application": "${APPLICATION_NAME}"
                }
            },
            "spec": {
                "strategy": {
                    "type": "Recreate"
                },
                "triggers": [
                    {
                        "type": "ImageChange",
                        "imageChangeParams": {
                            "automatic": true,
                            "containerNames": [
                                "${APPLICATION_NAME}-adminserver"
                            ],
                            "from": {
                                "kind": "ImageStreamTag",
                                "name": "${APPLICATION_NAME}:latest"
                            }
                        }
                    },
                    {
                        "type": "ConfigChange"
                    }
                ],
                "replicas": 1,
                "selector": {
                    "deploymentConfig": "${APPLICATION_NAME}-adminserver"
                },
                "template": {
                    "metadata": {
                        "name": "${APPLICATION_NAME}-adminserver",
                        "labels": {
                            "deploymentConfig": "${APPLICATION_NAME}-adminserver",
                            "application": "${APPLICATION_NAME}"
                        }
                    },
                    "spec": {
                        "terminationGracePeriodSeconds": 60,
                        "containers": [
                            {
                                "name": "${APPLICATION_NAME}-adminserver",
                                "image": "${APPLICATION_NAME}",
                                "imagePullPolicy": "Always",
                                "ports": [
                                    {
                                        "name": "http",
                                        "containerPort": 7001,
                                        "protocol": "TCP"
                                    }
                                ],
                                "livenessProbe": {
                                    "exec": {
                                        "command": [
                                            "cat",
                                            "/tmp/health"
                                        ]
                                    },
                                    "initialDelaySeconds": 120
                                },
                                "readinessProbe": {
                                    "exec": {
                                        "command": [
                                            "cat",
                                            "/tmp/health"
                                        ]
                                    }
                                },
                                "env": [
                                    
                                ],
                                "volumeMounts": [
                                    {
                                        "mountPath": "/oracle/domains/base_domain/config",
                                        "name": "${APPLICATION_NAME}-adminserver-pvol"
                                    }
                                ]
                            }
                        ],
                        "volumes": [
                            {
                                "name": "${APPLICATION_NAME}-adminserver-pvol",
                                "persistentVolumeClaim": {
                                    "claimName": "${APPLICATION_NAME}-adminserver-claim"
                                }
                            }
                        ]
                    }
                }
            }
        },
        {
            "apiVersion": "v1",
            "kind": "PersistentVolumeClaim",
            "metadata": {
                "labels": {
                    "application": "${APPLICATION_NAME}"
                },
                "name": "${APPLICATION_NAME}-adminserver-claim"
            },
            "spec": {
                "accessModes": [
                    "ReadWriteOnce"
                ],
                "resources": {
                    "requests": {
                        "storage": "50Mi"
                    }
                }
            }
        }
    ]
}
