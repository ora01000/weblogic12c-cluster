## Install Oracle WebLogic 12c(12.2.1.3.0)
## 2018.07.10 by Yun Insu ora01000@time-gate.com

FROM centos:centos7

MAINTAINER ora01000@time-gate.com


ENV SERVER_TYPE=adminserver \
	ADMINSERVER_URL=localhost:7001 \
	JAVA_HOME=/oracle/jdk1.8.0_171 \
	JAVA_VERSION=1.8.0_171 \
	JAVA_VENDOR=oracle \
	ORACLE_HOME=/oracle/middleware \
	MW_HOME=/oracle/middleware \
	WLS_HOME=/oracle/middleware/wlserver \
	WL_HOME=/oracle/middleware/wlserver \
	DOMAIN_HOME=/oracle/domains/base_domain \
	LOG_HOME=/oracle/logs \
	WLS_ADMIN=weblogic \
	WLS_PASSWD=weblogic123 \
	MIN_HEAP=1024 \
	MAX_HEAP=1024 \
	MIN_PERM=512 \
	MAX_PERM=512 \
	MIN_NEW=384 \
	MAX_NEW=384 \
	SURVIVOR_RATIO=9 \
	ADDITIONAL_JAVA_OPTIONS= \
	MIN_POOL_SIZE=50
	
ENV	PATH=${JAVA_HOME}/bin:/oracle/software/apache-maven-3.5.3/bin:$PATH \
	HOME=/home/weblogic \
	CLASSPATH=${CLASSPATH}:/oracle/middleware/wlserver/server/lib/startup.jar:/oracle/middleware/wlserver/server/lib/postgresql-42.1.1.jre7.jar
	
ENV WEBLOGIC_IMAGE_NAME="timegate/wls-cluster12c:12.2.1.3.0" \
    WEBLOGIC_IMAGE_VERSION="12.2.1.3.0" \
    WEBLOGIC_IMAGE_RELEASE="1" \
    STI_BUILDER="jee"
    
# Labels
LABEL name="$WEBLOGIC_IMAGE_NAME" \
      version="$WEBLOGIC_IMAGE_VERSION" \
      release="$WEBLOGIC_IMAGE_RELEASE" \
      architecture="x86_64" \
      com.redhat.component="oracle-weblogic-12c-docker" \
      io.k8s.description="Platform for building and running JavaEE applications on Oracle WebLogicServer 12c(12.2.1.3.0) Cluster" \
      io.k8s.display-name="Oracle WebLogicServer 12c cluster" \
      io.openshift.expose-services="7001:http" \
      io.openshift.tags="builder,javaee,weblogic,weblogic12c,cluster" \
      io.openshift.s2i.scripts-url="image:///usr/local/s2i"
		
USER 0

# yum install
RUN yum -y install wget net-tools bash-completion httpd-tools unzip && \
	yum clean all && \
#	groupadd -g 1001 -r weblogic && \
	useradd -u 1001 -r -m -g root weblogic -p 'weblogic' -d ${HOME}


RUN mkdir -p ${ORACLE_HOME} && mkdir -p ${JAVA_HOME} && mkdir -p ${LOG_HOME} && \
	mkdir -p /oracle/domains && mkdir -p ${HOME}/.m2 && chmod 777 ${LOG_HOME} && chmod -R 777 ${HOME} && \
	mkdir -p ${HOME}/deployments && chmod 777 ${HOME}/deployments && \
	mkdir -p /oracle/software && \
	chown -R weblogic.root /oracle && chown -R weblogic.root ${HOME}/.m2

COPY usr/local/s2i /usr/local/s2i

USER 1001



	
COPY settings.xml ${HOME}/.m2

#COPY jdk-8u171-linux-x64.tar.gz /oracle/jdk-8u171-linux-x64.tar.gz
RUN curl -o /oracle/jdk-8u171-linux-x64.tar.gz http://dns.ocp.tg.com/jdk-8u171-linux-x64.tar.gz && \
	gunzip /oracle/jdk-8u171-linux-x64.tar.gz && \
    cd /oracle && tar -xvf ./jdk-8u171-linux-x64.tar && \
	rm /oracle/jdk-8u171-linux-x64.tar
	
	
## WebLogic 12c silent install configuration

COPY wls.rsp /oracle/software/wls.rsp
COPY oraInst.loc /oracle/software/oraInst.loc

## WebLogic Silent Install
#COPY fmw_12.2.1.3.0_wls.jar /oracle/software/fmw_12.2.1.3.0_wls.jar
RUN curl -o /oracle/software/fmw_12.2.1.3.0_wls.jar http://dns.ocp.tg.com/fmw_12.2.1.3.0_wls.jar && \
	$JAVA_HOME/bin/java -Xmx1024m -jar /oracle/software/fmw_12.2.1.3.0_wls.jar -silent \
	-responseFile /oracle/software/wls.rsp -invPtrLoc /oracle/software/oraInst.loc && \
    rm /oracle/software/fmw_12.2.1.3.0_wls.jar && chmod -R 775 /oracle/middleware/wlserver
    
## WebLogic OPatch
## Currently(2018.02), 180116 patch applied
#RUN curl -o /oracle/software/p27438258_122130_Generic.zip http://dns.ocp.tg.com/p27438258_122130_Generic.zip && \
#	unzip -d /oracle/software/patch /oracle/software/p27438258_122130_Generic.zip  && \
#	cd /oracle/software/patch/27438258 && /oracle/middleware/OPatch/opatch apply -silent && \
#	rm -rf /oracle/software/patch && rm -rf /oracle/software/p27438258_122130_Generic.zip

# WebLogic OPatch
# Currently(2018.05), 180418 patch applied
RUN curl -o /oracle/software/p27342434_122130_Generic.zip http://dns.ocp.tg.com/p27342434_122130_Generic.zip && \
	unzip -d /oracle/software/patch /oracle/software/p27342434_122130_Generic.zip  && \
	cd /oracle/software/patch/27342434 && /oracle/middleware/OPatch/opatch apply -silent && \
	rm -rf /oracle/software/patch && rm -rf /oracle/software/p27342434_122130_Generic.zip

## Apache Maven Install
#COPY apache-maven-3.5.3-bin.tar.gz /oracle/software/apache-maven-3.5.3-bin.tar.gz
RUN curl -o /oracle/software/apache-maven-3.5.3-bin.tar.gz http://dns.ocp.tg.com/apache-maven-3.5.3-bin.tar.gz && \
	cd /oracle/software && gunzip ./apache-maven-3.5.3-bin.tar.gz && tar -xvf ./apache-maven-3.5.3-bin.tar && \
	rm /oracle/software/apache-maven-3.5.3-bin.tar && chmod -R 775 ./apache-maven-3.5.3 
	
## Make Domain - base_domain
COPY domain.py /home/weblogic/domain.py
RUN /oracle/middleware/wlserver/common/bin/wlst.sh /home/weblogic/domain.py && \
	cp /oracle/domains/base_domain/config/config.xml /oracle/domains/base_domain/config/config.xml.org

COPY container-scripts/scripts /oracle/domains/base_domain/scripts
COPY container-scripts/startA.sh /oracle/domains/base_domain
COPY postgresql-42.1.1.jre7.jar /oracle/middleware/wlserver/server/lib/postgresql-42.1.1.jre7.jar

## Startup Class
COPY startup.jar /oracle/middleware/wlserver/server/lib/startup.jar

## Deploy Startup Class && Init Cluster
## IMPORTANT!!! If upper layer is changed, (for examples, new PSU patch installed)
## config.xml should be changed new one for new encrypted password
COPY config/config.xml /oracle/domains/base_domain/config/config.xml


USER 0
RUN chown -R weblogic.root /oracle/domains/base_domain/scripts && \
	chown -R weblogic.root /oracle/domains/base_domain/startA.sh && \
	chmod -R 775 /oracle/domains && chmod 775 /oracle/middleware/wlserver/server/lib/postgresql-42.1.1.jre7.jar
	
USER 1001	

RUN mv /oracle/domains/base_domain/config /oracle/domains/base_domain/config_org

EXPOSE 7001

CMD ["/oracle/domains/base_domain/startA.sh"]
