readTemplate("/oracle/middleware/wlserver/common/templates/wls/wls.jar");

cd('Security/base_domain/User/weblogic');

set('Name','weblogic');

cmo.setPassword('weblogic123');

cd('/');

set('ProductionModeEnabled','true');

setOption('JavaHome','/oracle/jdk1.8.0_171');

cd('Servers/AdminServer');

set('Name','AdminServer');

cd('/');

writeDomain('/oracle/domains/base_domain');

closeTemplate();

exit()
